# Las guerras culturales y la vida en tiempos antiintelectuales

Treinta años atrás, la filósofa Judith Butler,[^*] ahora de 64
años, publicó un libro que revolucionó las actitudes populares
en torno al género. [_El género en disputa_](http://gen.lib.rus.ec/search.php?req=g%C3%A9nero+en+disputa&lg_topic=libgen&open=0&view=simple&res=25&phrase=1&column=def),
el trabajo por el que quizá es más conocida, introdujo las ideas
de género como acto performativo. En esta obra se pregunta sobre
cómo podemos definir «la categoría de mujer» y, en consecuencia,
qué es lo que el feminismo se propone luchar. En la actualidad
es un texto fundamental en cualquier lista de lecturas sobre
estudios de género, así como sus argumentos han cruzado de la
academia a la cultura popular.

En las tres décadas desde que _El género en disputa_ fuera publicado,
el mundo ha cambiado más allá del reconocimiento. En 2014, la
revista +++_TIME_+++ declaró el «[punto de inflexión transgénero](https://time.com/135480/transgender-tipping-point)».
Butler se ha transladado de  su obra temprana al escribir de manera
amplia sobre cultura y política. Pero los desacuerdos sobre el
esencialismo biológico todavía permanecen, como se evidenció
en las tensiones sobre los derechos de las personas trans en
el movimiento feminista.

¿Cómo es que Butler, quien es profesora “Maxine Elliot” en Literatura
Comparada en Berkeley, ve ahora este debate? ¿Acaso observa una
manera de salir del callejón sin salida? Butler recientemente
intercambió correos electrónicos con el portal _New Statesman_
acerca de este tema. El intercambio ha sido editado.

---

__Alona Ferber__ En _El género en disputa_, escribiste que «Los
debates feministas contemporáneos sobre los significados del
género conducen sin cesar a cierta sensación de problema o disputa,
como si la indeterminación del género, con el tiempo, pudiera
desembocar en el fracaso del feminismo». ¿Qué tanto las ideas
que exploraste en ese libro hace treinta años ayudan a explicar
el desplazamiento de los debates sobre los derechos de las personas
trans hacia las corrientes /principales/convencionales/ de la
cultura y la política?
{.sin-sangria}

__Judith Butler__ Primero quiero cuestionar si las feministas
transexclusionarias son realmente las mismas a las feministas
de las corrientes /principales/convencionales/. Si es correcta
tu identificación de las unas con las otras, entonces una posición
feminista que se opone a la transfobia es una posición marginal.
Pienso que esto puede ser equivocado. Mi apuesta es que la mayoría
de las feministas apoyan los derechos de las personas trans y
se oponen a todas las formas de transfobia. Así que me preocupa
que de repente la posición feminista transexclusionaria se entienda
como asepción comúnmente aceptada o incluso como la corriente
/principal/convencional/. Pienso que en realidad es un movimiento
marginal que pretende hablar a nombre de la corriente /principal/convencional/,
y que nuestra responsabilidad consiste en evitar que eso suceda.
{.sin-sangria .espacio-arriba1}

__AF__ Un ejemplo de discurso público presente en los medios
/principales/convencionales/ sobre este tema en Reino Unido es
el argumento acerca de la permisión de las personas a autoidentificarse
en términos de su género. En una carta abierta que publicó en
junio, J. K. Rowling articuló la preocupación de que esto podría
«abrir las puertas de los baños y los vestuarios a cualquier
hombre que cree o siente que es una mujer», lo que de manera
potencial pone a las mujeres en riesgos de violencia.
{.sin-sangria .espacio-arriba1}

__JB__ Si miramos de cerca al ejemplo que tú caracterizaste como
«presente en los medios /principales/convencionales/», podemos
ver que el dominio de la fantasía está trabajando, uno que refleja
más acerca de la feminista que tiene semejante miedo, que de
una situación en realidad existente en la vida de las personas
trans. La feminista que sostiene tal punto de vista supone que
el pene en efecto define a la persona, y que cualquiera con un
pene se identificaría como una mujer con el propósito de entrar
en esos vestuarios y representar una amenza para las mujeres
que están adentro. Esto asume que el pene es una amenaza, o que
una persona que tiene piene, así como identificarse con una mujer,
está empleando un disfraz cuya base es engañosa y dañina. Esta
es una rica fantasía, una que viene de /grandes/poderosos/ miedos,
pero que no describe la realidad social. Las mujeres trans por
lo general son discriminadas en los baños de los hombres y sus
modos de autoidentificación son maneras de describir una realidad
vivida, una que no puede ser capturada o regulada por las fantasías
que han sido puestas encima. El hecho de que tales fantasías
pasen como argumentos públicos es una causa de preocupación.
{.sin-sangria .espacio-arriba1}

__AF__ Quiero desafiarte con el término «+++TERF+++» o feminista radical
transexclusionaria (por sus siglas en inglés), que algunas personas
lo consideran un insulto.
{.sin-sangria .espacio-arriba1}

__JB__ No estoy al tanto de que +++TERF+++ se use como un insulto.
Me pregunto, ¿cómo debería llamarse a la autodeclarada feminista
que desea excluir a las mujeres trans de los espacios para las
mujeres? Si en realidad favorecen la exclusión, ¿por qué no llamarlas
exclusionarias? Si se entienden a sí mismas como parte de esa
cepa del feminismo radical que se opone a la reasignación del
género, ¿por qué no llamarlas feministas radicales? Mi único
arrepentimiento es que hubo un movimiento sobre la libertad sexual
radical, que alguna vez viajó bajo el nombre de feminismo radical,
pero que tristemente se ha transformado en una campaña que patolagiza
a las personas trans y no conformes. Tengo la sensación de que
tenemos que renovar el compromiso feminista sobre la equidad
social y la libertad de género para poder afirmar la complejidad
de las vidas empapadas de género tal como se viven hoy en día.
{.sin-sangria .espacio-arriba1}

__AF__ El consenso entre los progresistas parece ser que las
feministas que están del lado del argumento de J. K. Rowling
están en el lado equivocado de la historia. ¿Es esto justo o
hay algún mérito en sus argumentos?
{.sin-sangria .espacio-arriba1}

__JB__ Déjanos aclarar que el debate aquí no es entre las feministas
y las transactivistas. Hay feministas que afirman la cuestión
trans, así como también hay muchas personas trans que se comprometen
como feministas. Así que un problema claro es el encuadramiento
que actúa como si el debate es entre feministas y personas trans.
No lo es. Una razon para militar en contra de este encuadramiento
es debido a que el activismo trans está vinculado con el activismo
queer y con el legado feminista que permanece muy vivo hoy en
día. El feminismo siempre ha estado comprometido con la proposición
de que los significados sociales sobre qué es ser un hombre o
una mujer aún no está establecido. Nos dedicamos a contar historias
acerca de qué significa ser una mujer en un determinado tiempo
y lugar, así como damos seguimiento a la transformación de esas
categorías a través del tiempo.
{.sin-sangria .espacio-arriba1}

Dependemos del género a modo de categoría histórica y eso significa
que aún no sabemos todas las maneras de lo que puede significar,
además de que tenemos la apertura hacia nuevos entendimientos
de sus significados sociales. Para el feminismo sería un desastre
el regreso a un estricto entendimiento biológico del género o
a la reducción de la conducta social a una parte del cuerpo o
a la imposición de fantasías /aterradoras/temerosas/, sus propias
ansiedades sobre las mujeres trans… Su permanencia y sentido
muy real del género debe de ser social y públicamente reconocido
como una cuestión relativamente simple de concederle a otro la
dignidad humana. La posición feminista radical transexclusionaria
ataca la dignidad de las personas trans.

__AF__ En _El género en disputa_ preguntaste si, en la búsqueda
por representar una idea particular de mujer, las feministas
participan en la misma dinámica de opresión y heteronormatividad
que tratan de cambiar. A la luz de los amargos argumentos que
se están jugando ahora dentro del feminismo, ¿todavía aplica
lo mismo?
{.sin-sangria .espacio-arriba1}

__JB__ Según recuerdo, el punto del argumento en _El género en
disputa_ (escrito hace más de treinta años) era más bien diferente.
Primero, uno no tiene que ser mujer para ser feminista ni deberíamos
confundir las categorías. Los hombres feministas, los no binarios
y las transfeministas son parte del movimiento si se /adhieren/apegan/
a las proposiciones básicas de la libertad y la equidad que son
parte de cualquier lucha política feminista. Cuando las leyes
y las políticas sociales representan a las mujeres, estas llevan
a cabo decisiones tácitas acerca de quién cuenta como mujer,
y muy a menudo hacen presuposiciones acerca de lo que es ser
mujer. Esto lo hemos visto en el dominio de los derechos reproductivos.
Así que la pregunta que estaba haciendo era: ¿necesitamos tener
una idea /asentada/establecida/fija/ de la mujer, o cualquier
otro género, para poder avanzar en los objetivos feministas?
{.sin-sangria .espacio-arriba1}

La pregunta la planteé de esa manera… para recordarnos que las
feministas estamos comprometidas en pensar acerca de la diversidad
y los cambios históricos de los significados del género, así
como en los ideales de la libertad de género. Por libertad de
género no quiero decir que todos podemos elegir nuestro género.
En su lugar, conseguimos hacer un reclamo político en pos de
la vida en libertad y sin miedo a la descriminación ni violencia
en contra de todos los géneros que somos. Muchas personas cuyo
sexo asignado fue el «femenino» nunca se sintieron como en casa
con dicha asignación, y estas personas (entre las que me incluyo)
nos dicen a todos nosotros algo importante acerca de las limitaciones
de las tradicionales normas de género en el que muchas personas
caen fuera de esos términos.

Las feministas saben que la mujer con ambición es llamada «monstruo»
o que la mujer no heterosexual es patologizada. Nosotras luchamos
en contra de esas desfiguradas representaciones debido a que
son falsas y porque reflejan más acerca de la misoginia de quienes
hacen esas denigrantes caricaturas, que lo hacen acerca de la
compleja diversidad social de la mujer. Las mujeres no deberían
participar en las formas de las caricaturas fóbicas con las cuales
tradicionalmente han sido degradadas. Y por «mujeres» me refiero
a quienes se identifican de esa manera.

__AF__ ¿Cuánta toxicidad hay en este asunto en relación con las
guerras culturales que se están dando en internet?
{.sin-sangria .espacio-arriba1}

__JB__ Pienso que estamos viviendo en tiempos antiintelectuales
y que esto es evidente a lo largo de todo el espectro político.
La rapidez de las redes sociales permite formas de /vitrolio/hostilidad/críticas/
que no exactamente dan sostén a un debate /atento/reflexivo/.
Tenemos que tenerle aprecio a las formas más /largas/lentas/.
{.sin-sangria .espacio-arriba1}

__AF__ Las amenazas de violencia y abuso parece ser que toman
estos «tiempos antiintelectuales» a un extremo. ¿Qué nos dices
acerca del lenguaje violento o abusivo que se usa en internet
en contra de personas como J. K. Rowling?
{.sin-sangria .espacio-arriba1}

__JB__ Estoy en contra de cualquier abuso en internet. He de
confesar que me quedé perpleja por el hecho de que apuntas hacia
el abuso hecho en contra de J. K. Rowling, pero que no citaras
el abuso hecho en contra de las personas trans y sus aliados
que pasa en internet y en persona. Discrepo del punto de vista
de J. K. Rowling sobre las personas trans, pero no pienso que
debería sufrir acoso y amenazas. Aunque también hemos de recordar
las amenazas contra las personas trans en lugares como Brasil
y el acoso de personas trans en las calles y en el trabajo en
lugares como Polonia y Rumania ---o, más bien, aquí en los Estados
Unidos---. Así que si vamos a objetar el acoso y las amenazas,
que con seguridad deberíamos, también deberíamos asegurarnos
de que tenemos una imagen más grande de lo que está pasando,
quién es el que está afectado con mayor profundidad y si es tolerado
por quienes se supone debería oponerse. No vale decir que son
tolerables las amenazas en contra de algunas personas pero para
otras son intolerables.
{.sin-sangria .espacio-arriba1}

__AF__ Tú no firmaste la carta abierta sobre la «cultura de la
cancelación» publicada [en _Harper_](https://harpers.org/a-letter-on-justice-and-open-debate)
este verano, ¿acaso sus argumentos resuenan contigo?
{.sin-sangria .espacio-arriba1}

__JB__ Tengo sentimientos encontrados acerca de esa carta. Por
un lado, soy una educadora y escritora que cree en un debate
lento y reflexivo. Yo aprendí al ser confrontada y desafiada,
por lo que acepto que he cometido errores significativos en mi
vida pública. Si alguien entonces dice que no debería ser leída
o escuchada como resultado de mis errores, bueno, objetaría internamente
ya que no pienso que ningún error cometido por una persona pueda
o debería sintetizarla. Vivimos en el tiempo; erramos, a veces
de manera grave, y, si tenemos suerte, cambiamos precisamente
por esas interacciones que nos permitieron ver las cosas de manera
diferente.
{.sin-sangria .espacio-arriba1}

Por otro lado, algunas personas signatarias señalaron a Black
Lives Matter como si la oposición pública y escandalosa al racismo
fuera en sí misma un comportamiento incivilizado. Algunas de
estas se han opuesto a los derechos legales de Palestina. Otras
personas [alegadamente] han cometido abuso sexual. Y hay quienes
no quieren que su racismo sea desafiado. La democracia requiere
un buen desafío que no siempre llega con un tono suave. No estoy
a favor de la neutralización de las fuertes demandas políticas
por justicia de parte de las personas subyugadas. Cuando a alguien
no se le ha escuchado por décadas, la demanda por justicia está
destinada a hacer mucho ruido.

__AF__ En este año publicaste [_The Force of Nonviolence_](https://www.versobooks.com/books/3147-the-force-of-nonviolence).
La idea de «equidad radical», que discutes en ese libro, ¿es
relevante para el movimiento feminista?
{.sin-sangria .espacio-arriba1}

__JB__ Mi punto en este reciente libro es la sugerencia de repensar
la equidad en términos de interdependencia. Tendemos a decir
que una persona debería ser tratada de la misma manera que las
demás y medimos si esta equidad ha sido alcanzada o no al compararla
a partir de casos individuales. Pero ¿qué tal si el individuo
---y el individualismo--- es parte del problema? Hace una diferencia
el entendernos nosotros mismos como viviendo en un mundo donde
dependemos de manera fundamental de otros, de las instituciones,
de la Tierra, y el ver que esta vida depende de una red de apoyo
entre varias formas de vida. Si nadie escapa de esa interdependencia,
entonces somos equitativos en un sentido diferente. Somos dependientes
de manera equitativa, es decir, social y ecológicamente equitativas,
lo que quiere decir que cesamos de entendernos a nosotros mismos
solo como individuos demarcados. Si las feministas radicales
transexclusionarias se perciben a sí mismas como compartiendo
un mundo con las personas trans, bajo una lucha en común por
la equidad, para estar libres de violencia y por el reconocimiento
social, ya no serían feministas radicales transexclusionarias.
Aunque el feminismo con seguridad sobrevivirá como una coalición
de prácticas y visiones solidarias.
{.sin-sangria .espacio-arriba1}

__AF__ Tú has hablado acerca de la reacción en contra de la «ideología
de género», así como escribiste un esayo [para el _New Statesman_](https://www.newstatesman.com/2019/01/judith-butler-backlash-against-gender-ideology-must-stop)
sobre el asunto en 2019. ¿Ves alguna conexión entre esto y el
debate contemporáneo acerca de los derechos de las personas trans?
{.sin-sangria .espacio-arriba1}

__JB__ Duele ver que la posición de Trump, de que el género debería
ser definido por el sexo biológico, y que el evangélico y católico
esfuerzo de la derecha por purgar al «género» de la educación
y lapolítica pública sean afines al regreso del esencialismo
biológico de las feministas radicales transexclusionarias. El
día es triste cuando unas feministas promuevan la posición ideológica
antigénero de las fuerzas más reaccionarias en nuestra sociedad.
{.sin-sangria .espacio-arriba1}

__AF__ ¿Qué piensas que podría romper el callejón sin salida
en el feminismo sobre los derechos de las personas trans? ¿Qué
podría conducir a un debate más constructivo?
{.sin-sangria .espacio-arriba1}

__JB__ Supongo que un debate, en la medida de lo posible, tendría
que reconsiderar las manera en como la determinación médica del
sexo funciona en relación con la realidad vivida e histórica
del género.
{.sin-sangria .espacio-arriba1}

## Fuente

Traducción de «Judith Butler on the culture wars, JK Rowling
and living in “anti-intellectual times”» de Alona Ferber, publicado
en _New Statesman_ el 22 de septiembre del 2020. Disponible en
[alturl.com/ryw26](http://alturl.com/ryw26).

[^*]: Judith Butler emplea los pronombres «ella» o «nosotros».
