# De la edición con _software_ libre a la edición libre

Este *blog* es acerca de «edición libre» pero ¿qué quiere decir
eso? El término «libre» no es tan problemático en nuestra lengua
como en el inglés. En ese idioma existe una confusión entre «*free*»
como «barra libre» y como «libre discurso». Sin embargo, eso
no elimina el hecho de que el concepto de libertad es tan ambiguo
que incluso en Filosofía tratamos de usarlo con cuidado. Aunque
sea un problema, prefiero que el término no tenga una definición
clara; al final, ¿qué tan libres podríamos ser si la libertad
fuese bien definida?

Hace unos años, cuando empecé a trabajar codo a codo con Programando
Libreros y Hacklib, me di cuenta que no solo estábamos editando
con *software* libre. Estamos haciendo edición libre. Así que
intenté definirla en una publicación que ya no me convence.

El término siguió flotando alrededor hasta diciembre del 2018.
Durante el Contracorriente ---feria anual de *fanzine* celebrado
en Xalapa, México--- Hacklib y yo fuimos invitados a dar una
charla sobre edición y *software* libre. Entre todos hicimos
una cartulina de lo que hablamos aquel día.

![Cartulina hecha en el Contracorriente, ¿chigona, cierto?](../img/p001_i001.jpg)

La cartulina nos fue de mucha ayuda porque con un simple diagrama
de Venn pudimos distinguir varias intersecciones de actividades
que implican nuestro trabajo.

Así que no voy a definir qué es la edición, el *software* libre
o la política ---es mi perro *blog* así que puedo escribir lo
que quiera xD y siempre puedes usar el pato aunque sin respuesta
satisfactoria---. Como puedes ver, existen al menos dos intersecciones
muy familiares: las políticas culturales y el hacktivismo. No
sé cómo sea en tu país pero en México tenemos fuertes políticas
en pos de la publicación ---o al menos eso es lo que los editores
*piensan* y están cómodos con ello, sin importar que la mayoría
del tiempo es en detrimento del acceso abierto y de los derechos
de los lectores---.

«Hacktivismo» es un término difuso, pero es un poco más claro
si nos damos cuenta que el código como propiedad no es la única
forma en el que podemos definirlo. En realidad es una cuestión
muy problemática porque la propiedad no es un derecho natural,
sino uno producido en nuestras sociedades y resguardado por los
Estados ---sí, la individualidad no es el fundamento de los derechos
y las leyes, sino una construcción de la sociedad que a su vez
se produce a sí misma---. Entonces, ¿tengo que mencionar que
los derechos de propiedad no son tan justos como nos gustarían?

![Diagrama de Venn sobre edición, *software* libre y política.](../img/p001_i002_es.png)

Entre la edición y el *software* libre tenemos la «edición con
*software* libre». ¿Qué implica esto? Se trata de la acción de
publicar usando *software* que cumple con las famosas ---¿infames?---
cuatro libertades. Para las personas que usan al *software* como
herramienta esto quiere decir que, en primer lugar, no estamos
forzados a pagar para poder usarlo. Segundo, tenemos acceso al
código para poder hacer lo que queramos con él. Tercero ---y
lo más importante para mí---, podemos ser parte de una comunidad
en lugar de ser tratados como un consumidor.

¿Suena fantástico, cierto? Pero tenemos un problemita: la libertad
solo aplica al *software*. Como editor puedes beneficiarte del
*software* libre sin tener que liberar tu trabajo. Penguin Random
House ---el Google de la edición--- un día podría decidir usar
TeX o Pandoc con lo que se ahorraría un montón de dinero al mismo
tiempo que mantiene el monopolio en la edición.

Stallman vio este problema con los manuales publicados por O\'Reilly
y propuso la Licencia de documentación libre de +++GNU+++. Pero
al hacerlo de manera truculenta distinguió diferentes tipos de
obra. Es interesante ver al texto como función, cuestión de opinión
o estética, pero en la industria editorial a todos les vale un
comino. La distinción es muy buena entre escritores y lectores,
pero no problematiza el hecho de que son los editores quienes
deciden el rumbo de casi toda nuestra cultura texto-céntrica.

En mi opinión, esto es al menos peligroso. Así que prefiero otra
distinción truculenta. Las grandes editoriales y su rama mimética
---autodenominados edición «independiente»--- solo les importan
dos cosas: la venta y la reputación. Quieren vivir *bien* y obtener
el reconocimiento social por haber publicado *buenos* libros.
Si un día las comunidades de *software* libre desarrollan maquetadores
o sistemas de composición tipográfica fáciles de usar y aptos
para sus necesidades profesionales, vamos a ver una «repentina»
migración de la industria editorial al *software* libre.

Así que, ¿por qué no distinguimos las obras publicadas según
su financiamiento y sentido de comunidad? Si tu publicas con
recursos públicos ---para tu conocimiento, en México casi todo
lo que se publica tiene ese tipo de financiamiento---, sería
justo que liberaras los archivos y dejaras los impresos para
la venta: ya pagamos por ellos. Este es un argumento muy común
entre los que defienden el acceso abierto en la ciencia, pero
podemos ir más lejos. No importa que tu trabajo se sustente en
la funcionalidad, la opinión o la estética; si es un artículo
científico, un ensayo filosófico o una novela y tiene financiamiento
público, ¡venga!, ya hemos pagado por su acceso!

Todavía puedes vender publicaciones e ir a la Feria de Fráncfort,
la Feria Internacional del Libro de Guadalajara o la Feria del
Libro de Beijing: es solo hacer negocios con un *mínimo* de conciencia
social y política. ¿Por que quieres más dinero de nosotros si
ya te lo dimos? ---y además te llevas casi todas las ganancias
y dejas a las autoras con la mera satisfacción de ver publicada
su obra---…

Aquí cabe el sentido de comunidad. En un mundo donde uno de los
principales problemas es la escasez artificial ---muros de pago
en lugar de paredes reales\mbox{---,} nuestros trabajos publicados necesitan
licencias de *copyleft* o, mejor aún, de *copyfarleft*. No son
la solución, pero es un soporte que ayuda a mantener la libertad
y el acceso en la edición.

Con ese estado de las cosas, necesitamos herramientas libres
pero también ediciones libres. Ya cuento con las herramientas
pero carezco del permiso de publicar algunos libros que me gustan
mucho. No quiero que te pase eso con mi trabajo. Por eso necesitamos
un ecosistema donde tengamos acceso a todos los archivos de una
edición ---nuestro «código abierto» y «binarios»--- y a las herramientas
---el *software* libre--- para poder mejorar, como comunidad,
la calidad y el acceso de las obras y las habilidades necesarias.
¿Quién no quiere eso?

Con estas tensiones políticas, las herramientas que nos provee
el *software* libre y la edición como sustento de vida como editor,
escritor y lector, la edición libre es un camino. Con Programando
Libreros y Hacklib usamos *software* libre, invertimos tiempo
en activismo así como trabajamos en la edición: *hacemos edición
libre, ¿y tú?*
